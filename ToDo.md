# xindex

- add new table field for fist character of the sort entry. This is needed
  for entries like {\idxtextclasses!scrreprt|sffamily}, where the first
  sort part ist no more available to print it under D
  \def\idxtextclasses{Dokumentenklasse}   ---> DONE
  -- done
- pages get lost when Entry starts with backlslah like \TeXLive
  -- done
- key-val possibility for the xindex call
  -- done
- Allow another alphabetical list for the output. Use a 
  table like {Symbol, Number, A, B, C,...}
- different output for \indexentry{foo}{1} and \indexentry{foo|textit}{1}
  -- done